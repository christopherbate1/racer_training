import loadData
import torch
import util
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from util import GetRandomSubset, GetLogFilename, GetIndicesTrainTestSplit


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)


def ConductTraining(modelName, model, loss, EPOCHS=1, batch_size=1, validationSplit=0.2, logDir="./logs", debugPrint=False):
    '''
    modelName - how to label the log files
    model - pytorch model
    inputLen - number of  total bits
    parityBitSplit - number of bits which are used by random parity function
    '''
    logDir = GetLogFilename(modelName + "-", baseDir=logDir, createSubDir=True)

    # Create the datasets
    dataset = loadData.DrivingDataset("./data/train2/train_data.csv", "./")
    trainIndices, valIndices = GetIndicesTrainTestSplit(len(dataset), validationSplit)
    trainSampler = SubsetRandomSampler(trainIndices)
    valSampler = SubsetRandomSampler(valIndices)
    trainLoader = DataLoader(dataset, batch_size=batch_size, sampler=trainSampler)
    testLoader = DataLoader(dataset, batch_size=batch_size, sampler=valSampler)

    print("Size of dataset: ", len(dataset))

    net = model.to(device)
    optimizer = torch.optim.Adam(net.parameters())
    batchPrintInterval = 100
    loss_history = []
    loss_idx = []
    val_history = []
    val_idx = []

    runningIdx = 0
    finishEarly = False
    for epoch in range(EPOCHS):
        if(finishEarly):
            break
        running_loss = 0.0
        print("Epoch: ", epoch)
        for batch_idx, sample in enumerate(trainLoader):
            image_batch = sample['image'].to(device)
            controls_batch = sample['controls'].to(device)

            optimizer.zero_grad()

            outputs = net(image_batch)

            lVal = loss(outputs, controls_batch)
            lVal.backward()
            runningIdx += 1

            optimizer.step()

            running_loss += lVal.item()
            if (batch_idx % batchPrintInterval == (batchPrintInterval - 1)):
                print("Validating")
                with torch.no_grad():
                    if debugPrint:
                        print("{} {} loss {}".format(
                            epoch, batch_idx, running_loss))
                    loss_history.append(running_loss)
                    loss_idx.append(runningIdx)
                    running_loss = 0.0
                    print(loss_history)
            if (batch_idx % batchPrintInterval == (batchPrintInterval - 1)) or (batch_idx == len(trainLoader)-1):
                with torch.no_grad():
                    error_rate = 0.0
                    for sample in testLoader:
                        testImg = sample['image'].to(device)
                        target = sample['controls'].to(device)
                        res = net(testImg)
                        lVal = loss(res, target)
                        error_rate += lVal.item()
                    val_history.append(error_rate)
                    val_idx.append(runningIdx)
                    if(error_rate < 0.01):
                        finishEarly = True

        # Save Plots
        fig, axarr = plt.subplots(2, sharex=True)
        fig.suptitle("Training Loss (Top), Validation Performance (Bottom)")
        axarr[0].plot(loss_idx, loss_history, label="Training Loss")
        axarr[1].plot(val_idx, val_history, label="Validation")
        axarr[1].set(xlabel="Batch Number")
        fig.savefig(logDir + "/loss_val_fig" + modelName + ".png")
        plt.close(fig)
        print("Checkpoint model at epoch {} running index {} loss {} eval {}".format(epoch, runningIdx, loss_history[-1], val_history[-1]))
        torch.save(net.state_dict(), "{}/cp{}-model.pt".format(logDir, epoch))
