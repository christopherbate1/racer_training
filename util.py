from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import os


def HardThreshold(input, threshold, highBit=1.0, lowBit=0.0):
    input[input > threshold] = highBit
    input[input < threshold] = lowBit

    return input


def GetLogFilename(modelName, baseDir="./logs", createSubDir=True):
    startTime = datetime.now()
    if createSubDir:
        logDir = baseDir + "/" + modelName + startTime.strftime("%H-%M-%S-%d-%b")
    else:
        logDir = baseDir
    os.mkdir(logDir)
    return logDir


def PlotMetric(metric, filename, title):
    fig = plt.figure()
    plt.plot(metric)
    plt.title(title)
    fig.savefig(filename)
    plt.close(fig)


def GetRandomSubset(totalSet, subsetSplit):
    otherSet = totalSet
    split = int(np.floor(subsetSplit * len(totalSet)))
    np.random.shuffle(otherSet)
    randomSubset = otherSet[:split]
    return randomSubset


def GetIndicesTrainTestSplit(datasetLen, testSplit=0.2):
    allIndices = list(range(datasetLen))
    split = int(np.floor(testSplit * datasetLen))
    np.random.shuffle(allIndices)
    trainIndices, valIndices = allIndices[split:], allIndices[:split]
    return trainIndices, valIndices
