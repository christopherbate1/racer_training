import conv_net
import train
import torch.nn as nn

modelName = "colorDriveNet1"
model = conv_net.ConvNet()
loss = nn.MSELoss()
validationSplit = 0.2
logDir = "./logs"

train.ConductTraining(modelName, model, loss,
                      batch_size=10, EPOCHS=10, validationSplit=validationSplit, logDir=logDir)
