import torch
import torch.nn as nn
import torch.nn.functional as F


class ConvNetColor(nn.Module):
    def __init__(self, latent_dim=50, out_dim=2):
        self.latent_dim = latent_dim
        super(ConvNet, self).__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(3, 64, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(64, 128, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(128, 256, 3, stride=(2, 2))
        )

        self.fc1 = nn.Linear(386048, latent_dim)
        self.fc_output = nn.Linear(latent_dim, 2)

    def forward(self, x):
        x = self.layers(x)
        x = x.view(x.size(0), -1)        
        x = F.relu(self.fc1(x))
        x = self.fc_output(x)
        return x

class ConvNetDepth(nn.Module):
    def __init__(self, latent_dim=50, out_dim=2):
        self.latent_dim = latent_dim
        super(ConvNet, self).__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(3, 64, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(64, 128, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(128, 256, 3, stride=(2, 2))
        )

        self.fc1 = nn.Linear(386048, latent_dim)
        self.fc_output = nn.Linear(latent_dim, 2)

    def forward(self, x):
        x = self.layers(x)
        x = x.view(x.size(0), -1)        
        x = F.relu(self.fc1(x))
        x = self.fc_output(x)
        return x

class ConvNetDouble(nn.Module):
    def __init__(self, latent_dim=50, out_dim=2):
        self.latent_dim = latent_dim
        super(ConvNet, self).__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(3, 64, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(64, 128, 3, stride=(2, 2)),
            nn.ReLU(True),
            nn.Conv2d(128, 256, 3, stride=(2, 2))
        )

        self.fc1 = nn.Linear(386048, latent_dim)
        self.fc_output = nn.Linear(latent_dim, 2)

    def forward(self, x):
        x = self.layers(x)
        x = x.view(x.size(0), -1)        
        x = F.relu(self.fc1(x))
        x = self.fc_output(x)
        return x