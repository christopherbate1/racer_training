import torch
import numpy as np
from skimage import io, transform
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import matplotlib.pyplot as plt
import os


def ShowImage(image):
    plt.imshow(image)


class DrivingDataset(Dataset):
    def __init__(self, csv_file, root_dir, img_transform=None):
        self.root_dir = root_dir
        self.img_transform = img_transform
        self.dataraw = np.loadtxt(csv_file, dtype=str)
        self.filenames = self.dataraw[:, 2].astype(str)
        self.controls = torch.from_numpy(self.dataraw[:, 0:2].astype(np.float32))
        print(self.controls.dtype)
        self.normalize = transforms.Normalize(mean=[0.485], std=[0.229])

    def __len__(self):
        return self.dataraw.shape[0]

    def __getitem__(self, idx):
        imgfilename = self.filenames[idx]        
        image = io.imread(imgfilename).astype(np.float32)
        image = torch.from_numpy(image.transpose((2,0,1)))#[None, :, :]        
        image = self.normalize(image)
        sample = {'controls': self.controls[idx], 'image': image}
        return sample


if __name__ == '__main__':
    dataset = DrivingDataset("./data/train2/train_data.csv", "./", None)

    fig = plt.figure()

    for i in range(len(dataset)):
        sample = dataset[i]

        print(i, sample['image'].shape, sample['controls'])

        ShowImage(sample['image'][0,:,:])

        plt.show()
        if(i == 3):
            break
