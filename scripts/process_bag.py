import rosbag
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import os
import cv2
import numpy as np

filename = "./data/train2.bag"
outputDir = "./data/train2"
imagesDir = outputDir + "/images"
cmdFilename = "./data/train2/cmds.csv"
trainDataFilename = "./data/train2/train_data.csv"
startTime = 0

os.mkdir(outputDir)
os.mkdir(imagesDir)

bag = rosbag.Bag(filename)

bridge = CvBridge()

topics = bag.get_type_and_topic_info()[1].keys()
types = []


def PrintInfo():
    numDepth = bag.get_message_count("/camera/depth/image_rect_raw")
    numRGB = bag.get_message_count("/camera/color/image_raw")
    numCmd = bag.get_message_count("/racer/car_controller/command")
    print("{} depth {} rgb {} cmd".format(numDepth, numRGB, numCmd))
    info = bag.get_type_and_topic_info()[1]
    for j in info:
        print(j)
        print(info[j])
        print("")
    return (numDepth, numRGB, numCmd)


def readDepthImages():
    numDepth = bag.get_message_count("/camera/color/image_raw")
    imageArray = np.zeros((numDepth, 4))
    count = 0
    for topic, msg, t in bag.read_messages(topics=["/camera/color/image_raw"]):
        framename = "frame%08i.png" % count
        cv_img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
        cv2.imwrite(os.path.join(imagesDir, framename), cv_img)
        imageArray[count, :] = np.array([t.to_sec(), 0, 0, 0])
        count += 1
    return imageArray


numD, numC, numCmd = PrintInfo()


def ReadCommandData(numTotal):
    # Time, steering, throttle
    commandArray = np.zeros((numTotal, 3))
    count = 0
    for topic, msg, t in bag.read_messages(topics=["/racer/car_controller/command"]):
        commandArray[count, :] = np.array([t.to_sec(), msg.linear.x, msg.angular.z])
        count += 1
    return commandArray


def CreateTrainData(cmdData, imageData):
    cmdIndex = 0
    trainSize = cmdData.shape[0]
    # Train data: accel, steer, image filename
    trainArray = np.zeros((trainSize, 3), dtype=object)
    imageIndex = 0
    for i in range(cmdData.shape[0]):
        imageIndex = 0
        while imageData[imageIndex, 0] < cmdData[i, 0]:
            imageIndex += 1
            if(imageIndex > imageData.shape[0]):
                print("Images done, breaking")
                break
        print("Target Time: ", cmdData[i, 0], " Image Time: ", imageData[imageIndex, 0])
        print("Found image at index", imageIndex)
        imageIndex -= 1
        frameFile = os.path.join(imagesDir, "frame%08i.png" % imageIndex)
        print(frameFile)
        trainArray[i, :] = np.array(["%.3f" % (cmdData[i, 1]), "%.3f" % (cmdData[i, 2]), frameFile])
        print(trainArray[i, :])
    return trainArray


cmdArray = ReadCommandData(numCmd)
cmdArray = cmdArray[cmdArray[:, 0] > startTime, ]
np.savetxt(cmdFilename, cmdArray, fmt="%.3f")

imgArray = readDepthImages()
imgArray = imgArray[imgArray[:, 0] > startTime, ]
np.savetxt(trainDataFilename, imgArray, fmt="%.3f")

trnData = CreateTrainData(cmdArray, imgArray)
print(trnData[:10, :])
np.savetxt(trainDataFilename, trnData, "%s")

bag.close()
